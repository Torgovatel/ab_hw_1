import java.util.HashMap;
import java.util.List;

// Вспомогательный класс для укладки значений в мапу (задача 2)
public class Stacker
{
    Stacker(HashMap<Integer, Long> result_cont,
            List<Integer> cont)
    {
        this.result_cont = result_cont;
        this.cont = cont;
    }

    // Метод, укладывающий пару ключ - кол-во повторений
    // без учета одиночных элементов
    public void putWithoutZero(Integer key)
    {
        Long cnt = cont.stream().filter( x -> x == key).count();
        Long defaultValue = new Long(0);
        if (cnt>1 && result_cont.getOrDefault(key, defaultValue) == defaultValue)
            result_cont.put(key, cnt);
    }
    private HashMap<Integer, Long> result_cont;  // ссылка на мапу
    private List<Integer> cont;                  // ссылка на контейнер
}

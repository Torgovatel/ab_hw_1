import java.util.List;
import java.util.HashMap;

public class Main {

    // Метод, решающий задачу 1
    // 1. Написать метод, который принимает на вход список целых чисел, вернуть из этого метода список чисел,
    // где каждое число возведено в квадрат и к результату прибавлено 10. Требуется исключить из результирующего
    // списка все числа, которые заканчиваются на 5 или 6. Для решения задания требуется использовать Stream API
    public static List<Integer> square56(final List<Integer> cont)
    {
        if (cont == null)
            return null;
        // преобразуем список по условию задачи, выбрасываем числа оканчивающиеся на 5,6 и возвращаем результат
        return cont.stream().map(x -> x*x+10).filter(x -> x%10 !=5 && x%10 != 6).toList();
    }

    // Метод, решающий задачу 2
    // 2. Написать метод, который на вход принимает список чисел от 1 до 100. В данном списке существуют дубликаты.
    // Вернуть из метода мапу, в которой будет в качестве ключа числа, которое повторялось в списке, а значением –
    // сколько раз это число повторяется. Для решения задания требуется использовать Stream API
    public static HashMap<Integer, Long> getCountOfElements(final List<Integer> cont)
    {
        if (cont == null)
            return null;
        HashMap<Integer, Long> res = new HashMap<Integer, Long>();
        Stacker listToMap = new Stacker(res, cont);             // объект для упаковки значений в мапу из списка
        cont.stream().forEach(listToMap::putWithoutZero);
        return res;
    }

    public static void main(String[] args) {

    }
}
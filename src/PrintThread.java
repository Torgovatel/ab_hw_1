// Класс реализующий многопоточное поведение

public class PrintThread implements Runnable{

    PrintThread(int localNumber)
    {
        this.localNumber = localNumber;
    }
    @Override
    public void run()
    {
            System.out.println(localNumber);
    }
    private int localNumber;                // локальная цифра для вывода в данном потоке
}
